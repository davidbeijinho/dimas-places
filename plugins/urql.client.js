import urql from '@urql/vue';

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(urql, {
    url: 'https://fwx36jsv.directus.app/graphql',
  })
})

import { gql } from "urql/core";
import client from "./@/lib/graphql";

const QUERY = gql`
  query {
    place(filter: { status: { _eq: "published" } }) {
      id
      name
      picture {
        id
      }
      author {
        author_id {
          id
          name
        }
      }
    }
  }
`;

export default defineEventHandler(async (event) => {
  const result = await client.query(QUERY).toPromise();
  return result.data;
});

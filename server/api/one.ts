import { gql } from "urql/core";
import client from "./@/lib/graphql";

const QUERY = gql`
  query ($id: ID!) {
    place_by_id(id: $id) {
      id
      name
      description
      picture {
        id
      }
      coordinates
      year
      tags
      author {
        author_id {
          id
          name
        }
      }
    }
  }
`;

export default defineEventHandler(async (event) => {
  const query = useQuery(event);
  const result = await client.query(QUERY, { id: query.place }).toPromise();
  return result.data;
});

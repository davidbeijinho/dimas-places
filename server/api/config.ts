import { gql } from "urql/core";
import client from "./@/lib/graphql";

const QUERY = gql`
  query {
    Config {
      header_text
      large_text
      short_text
      avatar_title
      places_text
      places_title
      hero_image {
        id
      }
      avatar {
        id
      }
    }
  }
`;

export default defineEventHandler(async () => {
  const result = await client.query(QUERY).toPromise();
  return result.data;
});

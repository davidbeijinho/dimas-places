import { defineNuxtConfig } from "nuxt";

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/tailwindcss
    // "@nuxtjs/tailwindcss",
  ],
    // https://go.nuxtjs.dev/tailwindcss
  modules: ['@nuxtjs/tailwindcss'],
  alias: {
    'urql/core': 'urql/dist/urql.js'
  },
  ssr: false,
});

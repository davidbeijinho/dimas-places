import { default as urql }  from "urql/core";

const URL = "https://fwx36jsv.directus.app/graphql";

const client = urql.createClient({
  url: URL,
});

export default client;

import { gql } from "urql/core";

export default gql`
  query {
    countries {
      id
      name
    }
  }
`;

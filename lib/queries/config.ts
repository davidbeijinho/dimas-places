import { gql } from "urql/core";

export default gql`
  query {
    Config {
      header_text
      large_text
      short_text
      avatar_title
      places_text
      places_title
      hero_image {
        id
      }
      avatar {
        id
      }
    }
  }
`;

import { gql } from "urql/core";

export default gql`
  query {
    city {
      id
      name
    }
  }
`;

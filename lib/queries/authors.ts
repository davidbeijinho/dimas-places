import { gql } from "urql/core";

export default gql`
  query {
    author {
      id
      name
    }
  }
`;

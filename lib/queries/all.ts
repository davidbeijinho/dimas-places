import { gql } from "urql/core";

export default gql`
query ($filter: place_filter) {
  place(filter: $filter) {
      id
      name
      picture {
        id
      }
      author {
        author_id {
          id
          name
        }
      }
    }
  } 
`;

import { gql } from "urql/core";

export default gql`
  query ($id: ID!) {
    place_by_id(id: $id) {
      id
      name
      description
      picture {
        id
      }
      coordinates
      year
      tags
      author {
        author_id {
          id
          name
        }
      }
    }
  }
`;
